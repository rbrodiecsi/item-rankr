import pandas as pd
import sys


# Absolutely minimal example of PySide application with window

from PySide2.QtWidgets import *
from PySide2.QtCore import Signal, QObject


class PickerWidget(QDialog):
    """
    A widget for picking between a list of things by presenting them in pairs, one at a time.

    """
    choice = Signal(str, int)
    def __init__(self,parent=None):
        super(PickerWidget, self).__init__(parent)

        self.selectedSong = ""
        self.selectedValue = 0

        self.button1 = QPushButton("<<")
        self.button2 = QPushButton("<")
        self.button3 = QPushButton("-")
        self.button4 = QPushButton(">")
        self.button5 = QPushButton(">>")

        self.label1 = QLabel("Song A")
        self.label2 = QLabel("Song B")

        layout = QHBoxLayout()
        layout.addWidget(self.label1)
        layout.addWidget(self.button1)
        layout.addWidget(self.button2)
        layout.addWidget(self.button3)
        layout.addWidget(self.button4)
        layout.addWidget(self.button5)
        layout.addWidget(self.label2)

        self.button1.clicked.connect(lambda x: self.choice.emit(self.label1.text(), 2))
        self.button2.clicked.connect(lambda x: self.choice.emit(self.label1.text(), 1))
        self.button3.clicked.connect(lambda x: self.choice.emit("", 0))
        self.button4.clicked.connect(lambda x: self.choice.emit(self.label2.text(), 1))
        self.button5.clicked.connect(lambda x: self.choice.emit(self.label2.text(), 2))

        for b in [self.button1, self.button2, self.button3, self.button4, self.button5]:
            b.setFixedWidth(100)
            b.setFixedHeight(100)

        self.choice.connect(self.selected)

        self.setLayout(layout)
        self.setModal(True)

    def selected(self, song, value):
        self.selectedSong = song
        self.selectedValue = value
        self.accept()

    def compare(self, songA, songB):
        self.label1.setText(songA)
        self.label2.setText(songB)
        self.exec_()

        return self.selectedSong, self.selectedValue

    def go(self, list):
        from random import sample
        from collections import Counter

        reps = []

        list1 = sample(list,len(list))
        list2 = sample(list,len(list))
        comps = []
        for itemA in list1:
            for itemB in list2:
                if itemA>itemB:
                    comps.append([itemA,itemB])

        for items in sample(comps,len(comps)):
            song,value = self.compare(items[0],items[1])
            if value is None:
                return None
            if value>0:
                for i in range(value):
                    reps.append(song)
        output = pd.DataFrame(columns=['item','hits'])
        c = Counter(reps)


        output = pd.concat([pd.DataFrame({'item':item,'hits':c[item]} for item in list)])
        output.loc[:,'freq'] = output['hits'] / output['hits'].sum()
        return output


if __name__ == '__main__':
    # Get entrypoint through which we control underlying Qt framework
    app = QApplication([])

    window = PickerWidget()

    inputfile = QFileDialog.getOpenFileName(window,filter="*.txt",caption="Select a file to open")
    outputfile = QFileDialog.getSaveFileName(window, filter="*.csv", caption="Select an output file")
    if not inputfile is None and not outputfile is None:

        songs = pd.read_table(inputfile[0])

        # Qt automatically creates top level application window if you
        # instruct it to show() any GUI element

        rv = window.go(songs[songs.columns[0]].tolist())
        if not rv is None:
            rv.to_csv(outputfile[0])

    sys.exit(app.exec_())